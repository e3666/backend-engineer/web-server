# SUMÁRIO

- [**Web Server**](#web-server)
- [**Como Funciona**](#como-funciona)
    - [Blocking Single-Threaded Web Server](#blocking-single-threaded-web-server)
- [**Apache**](#apache)
- [**Python-Flask**](#python-flask)
- [**Referência**](#referência)


# WEB SERVER

Um Web Server é um software que fornece conteúdo através do protocolo http, eles fornecem conteúdos estáticos e dinâmicos.

Hospedando APIs, Blogs, Web Pages, etc.

# Como Funciona

Como o protocolo de comunicação é o HTTP então todo o funcionamento é idêntico ao explicado nó conteúdo de HTTP que escrevi [aqui](https://gitlab.com/e3666/backend-engineer/communication-protocols#http)

## Blocking Single-Threaded Web Server

Um web server que funciona dessa forma consegue atender somente um client por vez, pois ele possui somente um thread.

Ou seja, quando um cliente efetua uma request ele abre um socket TCP para esse cliente, resolve seu processo e retorna o resultado. Porém se outro client fizer uma request durante o processo do primeiro o servidor irá criar o socket, porém ele terá que aguardar para ser processado quando for liberado.

# Apache

Para resolver esse problema existem algumas abordagens, o Apache por exemplo cria uma thread para cada request que recebe, sendo que você define um número máximo de threads que podem existir. Quanto mais memória e processamento a máquina tiver, mais processos simultâneos você pode colocar para o apache criar.

Para exemplificar o apache, irei criar um servidor apache utilizando docker. Executando o comando:

```sh
$ ./01-Basic/apache.sh
```

Após o container estár ativo, você pode acessar `http:localhost:8080` e verá a pagina html que o apache deixa de exemplo dentro de `/usr/local/apache2/htdocs`. E todos os arquivos .html, .php, .js... que você colocar entro dessa pasta (no container) o apache irá servir para você.

> OBS.: O local padrão para o apache servir é `/var/www/html` o endereço acima é por conta de ser um container.

# Python-Flask

Irei criar um exemplo com flask para servir um html simples. Então instale o Flask na sua máquina ou venv, e execute o comando abaixo e acesse na a url: `http://127.0.0.1:5000`:

```sh
$ python /01-Basic/server.py
```

# Referência
- [What are web servers and how do they work (with examples httpd and nodejs) - Hussein Nasser](https://www.youtube.com/watch?v=JhpUch6lWMw&list=PLQnljOFTspQUNnO4p00ua_C5mKTfldiYT&index=8)